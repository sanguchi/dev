extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal status_changed(new_status)
var status: int = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	$SpriteStatus.visible = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var old_status = status
	$SpriteStatus.visible = false
	var bodies: Array = $Area2D.get_overlapping_bodies()
	if(not bodies):
		status = 0
	for body in bodies:
		if(body as Player):
			$SpriteStatus.visible = true
			if(Input.is_action_pressed("direction_up")):
				$SpriteStatus.frame = 2
				status = -1
				emit_signal("status_changed", -1)
			elif(Input.is_action_pressed("direction_down")):
				$SpriteStatus.frame = 1
				status = 1
				emit_signal("status_changed", 1)
			else:
				status = 0
				$SpriteStatus.frame = 0
	if(old_status != status):
		emit_signal("status_changed", 0)
