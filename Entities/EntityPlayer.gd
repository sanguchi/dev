extends KinematicBody2D
class_name Player
const TARGET_FPS = 60
const ACCELERATION = 14
const MAX_SPEED = 70
const FRICTION = 16
const AIR_RESISTANCE = 4
const GRAVITY = 8
const JUMP_FORCE = 140

var motion = Vector2.ZERO
var direction_facing = Vector2.ZERO

onready var sprite = $Sprite
onready var animationPlayer = $AnimationPlayer
onready var overlay = $PivotCenter/Pivot/SpriteOverlay
onready var pivot = $PivotCenter
var vector_input = Vector2.ZERO
export (bool) var paused = false
func _process(_delta):
	if(paused):
		return
	sprite.flip_h = direction_facing.x < 0
	if(vector_input.x != 0):
		animationPlayer.play("animation_run")
	else:
		animationPlayer.stop()
		sprite.frame = 0
	if(not is_on_floor()):
		animationPlayer.play("animation_jump")


func _physics_process(delta):
	if(paused):
		return
	var x_input = Input.get_action_strength("direction_right") - Input.get_action_strength("direction_left")
	var y_input = Input.get_action_strength("direction_down") - Input.get_action_strength("direction_up")
	vector_input = Vector2(x_input, y_input)
	if x_input != 0:
		motion.x += x_input * ACCELERATION * delta * TARGET_FPS
		motion.x = clamp(motion.x, -MAX_SPEED, MAX_SPEED)
	adjust_pivot(x_input, y_input)
	
	# Apply gravity
	motion.y += GRAVITY * delta * TARGET_FPS
	
	if is_on_floor():
		if x_input == 0:
			motion.x = lerp(motion.x, 0, FRICTION * delta)
			
		if Input.is_action_just_pressed("action_jump"):
			motion.y = -JUMP_FORCE
	else:
		if Input.is_action_just_released("action_jump") and motion.y < -JUMP_FORCE / 2.0:
			motion.y = -JUMP_FORCE / 2.0
		
		if x_input == 0:
			motion.x = lerp(motion.x, 0, AIR_RESISTANCE * delta)
	
	# motion.y = lerp(0, 70, motion.y)
	motion = move_and_slide(motion, Vector2.UP, false, 4, PI/4, false)
	
	push_objects()


func push_objects():
	# Push objects
	for index in get_slide_count():
		var collision = get_slide_collision(index)
		if collision.collider.get_class() == ("RigidBody2D"):
			collision.collider.apply_central_impulse(-collision.normal * 4)
		# print(collision.collider.get_class())


func adjust_pivot(x_input, y_input):
	# Adjust pivot if up/down is pressed
	if(y_input != 0):
		direction_facing.y = y_input
		# If left_righ are pressed too, apply diagonals
		if(x_input != 0):
			direction_facing.x = x_input
		else:
			direction_facing.x = 0
			if(y_input > 0):  # Check down key pressed
				# -4 not fliped -1 fliped when down pressed only
				$PivotCenter/Pivot/SpriteOverlay.position.y = -1 if sprite.flip_h else -4
			# Check only up key is pressed
			else:
				# -4 flipped -1 not fliped
				$PivotCenter/Pivot/SpriteOverlay.position.y = -4 if sprite.flip_h else -1
				
	# restore side angles if no vertical input
	else:
		direction_facing.y = 0
		
		$PivotCenter/Pivot/SpriteOverlay.position.y = -3 if sprite.flip_h else -2
		if(x_input != 0):
			direction_facing.x = x_input
		else:
			direction_facing = Vector2.LEFT if sprite.flip_h else Vector2.RIGHT
	# Rotate pivot and adjust overlay
	# $PivotCenter.rotation_degrees = 0 if not sprite.flip_h else 180
	$PivotCenter.rotation = direction_facing.angle()
	# print($PivotCenter.rotation)
