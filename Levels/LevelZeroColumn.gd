extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var is_up: bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if(get_tree().current_scene == self):  # Debug scene
		var player = load("res://Entities/EntityPlayer.tscn").instance()
		player.position = $Position2D.position
		add_child(player)



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# FIX THIS COLLISION WITH TILEMAP
func _on_Switch_status_changed(new_status):
	if(new_status):
		$ELEVATOR/AnimationPlayer.play("platform_up")
	else:
		$ELEVATOR/AnimationPlayer.play_backwards("platform_up")
