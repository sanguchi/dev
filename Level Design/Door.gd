extends Node2D
# LOGIC TYPE, NO EMIT, LINKED TYPE


export (NodePath) var linked_door

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.visible = false
	assert(linked_door)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	$Label.visible = false
	if($AnimationPlayer.current_animation == "door_open"):
		return
	var touching_nodes: Array = $Area2D.get_overlapping_bodies()
	for touching_node in touching_nodes:
		if(touching_node as Player):
			$Label.visible = true
			# Check key input
			if(Input.is_action_just_pressed("direction_down")):
				$Label.visible = false
				var target_door = get_node(linked_door)
				touching_node.position = target_door.global_position + Vector2(0, 5) 
				target_door.get_node("AnimationPlayer").play("door_open")
				$AnimationPlayer.play("door_open")
				
				# yield($AnimationPlayer, "animation_finished")
				# print("Animation finished sprite index ", $Sprite.frame)
			if(not $AnimationPlayer.is_playing()):
				$AnimationPlayer.play("label_float")
