tool
extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (NodePath) var target_tilemap
export (NodePath) var listen_to
export (int) var cell_index_on: int = 0
export (bool) var preserve_tile_index: bool = false

var status: bool = true
var tiles: Array = Array()
var tiles_fliped: Array = Array()
# signal status_changed(new_status)

# Called when the node enters the scene tree for the first time.
func _ready():
	if(not Engine.editor_hint):
		assert(listen_to and get_node(listen_to).has_signal("status_changed"))
		var _err = get_node(listen_to).connect("status_changed", self, "status_changed_listener")
		assert(target_tilemap)
		var tilemap_node = get_node(target_tilemap)
		for child in get_children():
			if(child is Position2D):
				var tile_index = (tilemap_node as TileMapSolid).get_cell_from_global(child.global_position)
				var tile_flip_x = (tilemap_node as TileMapSolid).get_flip_x_from_global(child.global_position)
				var tile_flip_y = (tilemap_node as TileMapSolid).get_flip_y_from_global(child.global_position)
				if(preserve_tile_index):
					tiles.append(tile_index)
					tiles_fliped.append(Vector2(tile_flip_x, tile_flip_y))
				else:
					tiles.append(cell_index_on)
					tiles_fliped.append(Vector2(0, 0))
	# pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(Engine.editor_hint):
		update()


func _draw():
	if(Engine.editor_hint):
		var view = get_viewport().size
		for child in get_children():
			if(child is Position2D):
				# draw_string(load("res://Resources/PixelFont.tres"), (global_position / get_viewport().size) - Vector2(16, -4), "[TT]") 
				draw_line(global_position / view, child.position, Color.olivedrab)
		if(listen_to):
			var n = get_node(listen_to)
			draw_line(global_position / view, to_local(n.global_position), Color.cadetblue)

func status_changed_listener(new_status: bool):
	action(new_status)
	
func action(new_status: bool):
	# print("Action")
	assert(target_tilemap)
	var tilemap_node = get_node(target_tilemap)
	status = new_status
	# emit_signal("status_changed", new_status)
	var i = 0
	for child in get_children():
		if(child is Position2D):
			if(status):
				(tilemap_node as TileMapSolid).set_cell_from_global(child.global_position, tiles[i], tiles_fliped[i].x, tiles_fliped[i].y)
			else:
				(tilemap_node as TileMapSolid).set_cell_from_global(child.global_position, -1)
			i += 1
	
			
			
