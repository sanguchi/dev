extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var camera: Camera2D = $Camera2D
var following_player: bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
	var _err = connect("body_entered", self, "player_enter")
	var _erx = connect("body_exited", self, "player_exit")
	# assert ($CollisionShape2D.shape)
	# print("Asserted ", $CollisionShape2D.shape)
	for node in get_children():
		if(node is CollisionShape2D):
			# print("Shape found")
			var rect_shape: RectangleShape2D = node.shape
			var viewport: Viewport = get_viewport()
			
			if(rect_shape.extents.y >= viewport.size.y / 2):  # Camera fits vertically inside rectangle shape
				# Enable vertical camera movement
				camera.limit_top = node.global_position.y - rect_shape.extents.y
				camera.limit_bottom = node.global_position.y + rect_shape.extents.y
			else:  # If not, disable vertical movement
				camera.limit_top = global_position.y - viewport.size.y / 2
				camera.limit_bottom = global_position.y + viewport.size.y / 2
				
			if(rect_shape.extents.x >= viewport.size.x / 2):  # Camera fits horizontally inside rectangle shape
				# Enable horizontal camera scroll
				camera.limit_left = node.global_position.x - rect_shape.extents.x
				camera.limit_right = node.global_position.x + rect_shape.extents.x
			else:  # If not, disable horizontal movement
				camera.limit_left = global_position.x - viewport.size.x / 2
				camera.limit_right = global_position.x + viewport.size.x / 2
				
			
#	


func player_enter(body: Player):
	if(body):
		# print("Player enter zone")
		remove_child(camera)
		body.add_child(camera)
		camera.current = true
	

func player_exit(body: Player):
	if(body):
		# print("Player exit")
		body.remove_child(camera)
		add_child(camera)
		# camera.current = false
