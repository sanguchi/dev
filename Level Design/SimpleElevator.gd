extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# export (Path2D) var line
var status = 0
var motion = Vector2.ZERO

const ACCELERATION = 28
const MAX_SPEED = 70
const FRICTION = 16
const TARGET_FPS = 60
export (NodePath) var player_node
onready var tilemap = $TileMap
# Called when the node enters the scene tree for the first time.
func _ready():
	assert(player_node)
	var player_instance = get_node(player_node)
	assert(player_instance is Player)
	
	# add_collision_exception_with(player_instance)
	# add_collision_exception_with(tilemap)
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	motion = Vector2.ZERO
	if(status):
		var collision = false
		var bodies: Array = Array()
		if(status == 1):  # DOWN
			bodies = $AreaDOWN.get_overlapping_bodies()
		elif(status == -1):  # UP
			bodies = $AreaUP.get_overlapping_bodies()
		
		for body in bodies:
			if(body is TileMapSolid):
				collision = true
				
		if(not collision):
			
			motion.y += status * ACCELERATION * delta * 10
			motion.y = clamp(motion.y, -MAX_SPEED, MAX_SPEED)
			if(status == -1):
				get_node(player_node).position.y += motion.y
				position += motion
			elif(status == 1):
				get_node(player_node).position.y += motion.y
				position += motion
			
	
#	
#	if status:
#		
#	else:
#		motion.y = lerp(motion.y, 0, FRICTION * delta)
#	# TODO: ADD PLAYER TO MOVEMENT CALC
#	var player_instance = get_node(player_node)
#	motion = move_and_slide(motion, Vector2.ZERO)
#	for index in range(get_slide_count()):
#		var collision = get_slide_collision(index)
#		if collision.collider.get_class() == ("KinematicBody2D"):
#			collision.collider.apply_central_impulse(-collision.normal * 4)
#	# var collision_info = move_and_collide(motion)
#	# if(collision_info):
#	#	print((collision_info as KinematicCollision2D).collider)
#
	
	

func _on_TripleSwitch_status_changed(new_status):
	status = new_status
