tool
extends Sprite


# LOGIC TYPE EMIT SIGNALS AND TARGETS SHOULD LISTEN TO THEM

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal status_changed(new_status)

export (bool) var status: bool = false setget set_status

func set_status(new_status):
	status = new_status
	emit_signal("status_changed", status)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	frame = status

