extends Area2D

# Called when the node enters the scene tree for the first time.
var collision_shape: CollisionShape2D  # Sets later.


func _ready():
	for node in get_children():
		if(node is CollisionShape2D):
			collision_shape = node


func _process(_delta):
	for body in get_overlapping_bodies():
		if(body as Player):
			var camera: Camera2D = body.get_node("Camera2D")
			camera_set_limits(camera)


func camera_set_limits(camera: Camera2D):
	assert(collision_shape)
	var rect_shape: RectangleShape2D = collision_shape.shape
	var viewport: Viewport = get_viewport()
	if(rect_shape.extents.y >= viewport.size.y / 2):  # Camera fits vertically inside rectangle shape
		# Enable vertical camera movement
		camera.limit_top = collision_shape.global_position.y - rect_shape.extents.y
		camera.limit_bottom = collision_shape.global_position.y + rect_shape.extents.y
	else:  # If not, disable vertical movement
		camera.limit_top = global_position.y - viewport.size.y / 2
		camera.limit_bottom = global_position.y + viewport.size.y / 2
	if(rect_shape.extents.x >= viewport.size.x / 2):  # Camera fits horizontally inside rectangle shape
		# Enable horizontal camera scroll
		camera.limit_left = collision_shape.global_position.x - rect_shape.extents.x
		camera.limit_right = collision_shape.global_position.x + rect_shape.extents.x
	else:  # If not, disable horizontal movement
		camera.limit_left = global_position.x - viewport.size.x / 2
		camera.limit_right = global_position.x + viewport.size.x / 2
