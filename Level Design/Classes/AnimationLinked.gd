extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (String) var animation_default
export (String) var animation_true
export (String) var animation_false
export (bool) var single_animation
export (NodePath) var node_to_listen
var animation_player: AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	assert(node_to_listen and get_node(node_to_listen).has_signal("status_changed"))
	get_node(node_to_listen).connect("status_changed", self, "status_changed_listener")
	if(single_animation):
		assert(animation_default)
	else:
		assert(animation_false and animation_true)
		
	for child in get_children():
		if(child as AnimationPlayer):
			animation_player = child
			return
	assert(0, "No child AnimationPlayer found")


func status_changed_listener(new_status):
	# print("Status ", new_status)
	if(single_animation):
		animation_player.play(animation_default)
	else:
		if(new_status):
			animation_player.play(animation_true)
		else:
			animation_player.play(animation_false)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
