extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$RayCast2Up.add_exception($StaticBody2D)
	$RayCastDown.add_exception($StaticBody2D)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(_delta):
	$LightBulbDown.status_changed_listener($RayCastDown.is_colliding())
	$LightBulbUp.status_changed_listener($RayCast2Up.is_colliding())
	if($RayCast2Up.is_colliding()):
		pass
		# print($RayCast2Up.get_collider())


func _on_TripleSwitch_status_changed(new_status: int):
	if(new_status == 1):  # DOWN
		
		position.y += .5
	elif(new_status == -1):  # UP
		position.y -= .5
	else:
		pass
