extends Node2D
# LOGIC TYPE EMIT SIGNALS AND TARGETS SHOULD LISTEN TO THEM

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal status_changed(new_status)


export (bool) var status: bool = false
export (bool) var use_once: bool = false
var used: bool = false
# export (Array, NodePath) var target_nodes: Array = Array() 
var target_nodes: Array = Array()

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.visible = false
	$SpriteIndicator.frame = 2 if status else 3
	$Sprite.frame = not status
	# assert(not target_nodes.empty())
	# notify_targets()
	emit_signal("status_changed", status)
	
	
func notify_targets():
	# print("Notifiy targets")
	for target in target_nodes:
		var target_node = get_node(target)
		assert(target_node.has_method("action"))
		target_node.action(status)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	$Label.visible = false
	if(use_once and used):
		return
	if($AnimationPlayer.current_animation == "label_on_off"):
		return
	var touching_nodes: Array = $Area2D.get_overlapping_bodies()
	for touching_node in touching_nodes:
		if(touching_node as Player):
			$Label.visible = true
			# Check key input
			if(Input.is_action_just_pressed("direction_down")):
				# if($AnimationPlayer.is_playing() and $AnimationPlayer.current_animation == "label_on_off")
				status = not status
				emit_signal("status_changed", status)
				if(use_once):
					used = true
				# notify_targets()
				$SpriteOnOff.frame = status
				$AnimationPlayer.play("label_on_off")
				# yield($AnimationPlayer, "animation_finished")
			if(not $AnimationPlayer.is_playing()):
				$AnimationPlayer.play("label_float")
				
	if(not $Label.visible):
		$AnimationPlayer.stop()
	# 2 on 3 off
	$SpriteIndicator.frame = 3 if status else 2
	$Sprite.frame = not status

