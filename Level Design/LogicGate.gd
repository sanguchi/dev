tool
extends Node2D

# Optional, node to call action() or connect to status_changed_listener
export (NodePath) var target_node
# Input nodes to subscribe
export (NodePath) var input_A
export (NodePath) var input_B

# Uses Logic but need to input to connect
# Tipo de suscripcion, programada
signal status_changed(new_status)
enum OperationMode {NEGATE, AND, OR}
export (OperationMode) var operation = OperationMode.AND
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var ax
var bx
var status_a
var status_b
# onready var out = get_node(target_node)
var status: bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
	if(target_node):
		var out = get_node(target_node)
		assert(out.has_method("action") or out.has_method("status_changed_listener"))
		if(out.has_method("status_changed_listener")):
			connect("status_changed", out, "status_changed_listener")
			
	if(operation == OperationMode.NEGATE):
		assert(input_A or input_B)
		if(input_A):
			ax = get_node(input_A)
			assert(ax.has_signal("status_changed"))
			ax.connect("status_changed", self, "on_input_A_change")
		else:
			bx = get_node(input_B)
			assert(bx.has_signal("status_changed"))
			bx.connect("status_changed", self, "on_input_B_change")
			
	else:
		assert(input_A and input_B)
		ax = get_node(input_A)
		bx = get_node(input_B)
		assert(ax.has_signal("status_changed"))
		assert(bx.has_signal("status_changed"))
		ax.connect("status_changed", self, "on_input_A_change")
		bx.connect("status_changed", self, "on_input_B_change")
	if(ax):
		status_a = ax.status
	if(bx):
		status_b = bx.status


func update_status():
	var old_status = status
	if(operation == OperationMode.NEGATE):
		if(ax and status == status_a):
			status = not status_a
		elif(bx and status == status_b):
			status = not status_b
	elif(operation == OperationMode.AND):
		status = status_a and status_b
	elif(operation == OperationMode.OR):
		status = status_a or status_b
	if(status != old_status):
		emit_signal("status_changed", status)
		if(target_node):
			var out = get_node(target_node)
			if(out.has_method("action") and not out.has_method("status_changed_listener")):
				out.action(status)
			


func on_input_A_change(new_status: bool):
	status_a = new_status


func on_input_B_change(new_status: bool):
	status_b = new_status

	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	update_status()
	$Sprite.frame_coords.y = 0
	if(operation == OperationMode.NEGATE):
		$Sprite.frame_coords.x = 2
		
	elif(operation == OperationMode.AND):
		$Sprite.frame_coords.x = 1
	elif(operation == OperationMode.OR):
		$Sprite.frame_coords.x = 0
	
	if(ax and status_a):
		$Sprite.frame_coords.y += 1
	if(bx and status_b):
		$Sprite.frame_coords.y += 1
	$Sprite.flip_v = true if status_a else false

func action(new_status):
	pass
