tool
extends Line2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (NodePath) var listen_to

var status: bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
	assert(listen_to and get_node(listen_to).has_signal("status_changed"))
	get_node(listen_to).connect("status_changed", self, "status_changed_listener")
	

func _get_configuration_warning():
	if(listen_to):
		return ""
	return "Set a node to listen for status changes"


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	# print(points[0].x == floor(points[0].x))
	# print(points[0].x)
	# if(points[0].x == floor(points[0].x)):
		# points[0].x += 0.5
	for i in range(points.size()):
		if(points[i].x == floor(points[i].x)):
			points[i].x += 0.5
		if(points[i].y == floor(points[i].y)):
			points[i].y += 0.5

	if(status):
		default_color = "#ffffff"
	else:
		default_color = "#7f7f7f"

func status_changed_listener(new_status: bool):
	status = new_status
