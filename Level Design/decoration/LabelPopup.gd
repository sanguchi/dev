tool
extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var visible_rect: CollisionShape2D
export (String) var label_text: String = "DEFAULT TEXT"
export (bool) var always_visible: bool = false
export (bool) var uppercase: bool = true


# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = label_text
	$Label.uppercase = uppercase
	for point in $Line2D.points:
		point.x = 0
		point.y = 0
	if(always_visible):
		return
	for child in get_children():
		if(child as CollisionShape2D):
			visible_rect = child
	assert(visible_rect)
	
	
func _get_configuration_warning():
	if(not always_visible and visible_rect == null):
		return "LabelPopup requires a CollisionShape2D"
	return ""

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	$Label.text = label_text
	$Label.uppercase = uppercase
	$Label.rect_size = Vector2.ZERO
	# var label_width = get_total_character_count() * 4
	# var label_height = get_line_height()
	# print(get_line_height())
	$Line2D.points[0].x = -2
	$Line2D.points[0].y = -1.5
	$Line2D.points[1].x = $Label.rect_size.x + 0.5
	$Line2D.points[1].y = -1.5
	$Line2D.points[2].x = $Label.rect_size.x + 0.5
	$Line2D.points[2].y = $Label.rect_size.y + 1.5
	$Line2D.points[3].x = -1.5
	$Line2D.points[3].y = $Label.rect_size.y + 1.5
	$Line2D.points[4].x = -1.5
	$Line2D.points[4].y = -1.5
	
func _physics_process(_delta):
	if(Engine.editor_hint):
		return
	if(always_visible):
		return
	visible = false
	var bodies: Array = get_overlapping_bodies()
	for body in bodies:
		if(body as Player):
			visible = true
	
