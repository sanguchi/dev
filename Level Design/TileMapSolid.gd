extends TileMap
class_name TileMapSolid

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func get_cell_from_global(global_pos: Vector2):
	return get_cellv(world_to_map(to_local(global_pos)))
	
func set_cell_from_global(global_pos: Vector2, cell_value: int, flip_x = false, flip_y = false):
	set_cellv(world_to_map(to_local(global_pos)), cell_value, flip_x, flip_x)
	
func get_flip_y_from_global(global_pos: Vector2):
	var tile_vector: Vector2 = world_to_map(to_local(global_pos))
	return is_cell_y_flipped(tile_vector.x, tile_vector.y)

func get_flip_x_from_global(global_pos: Vector2):
	var tile_vector: Vector2 = world_to_map(to_local(global_pos))
	return is_cell_x_flipped(tile_vector.x, tile_vector.y)
