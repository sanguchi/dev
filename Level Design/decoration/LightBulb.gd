tool
extends Sprite

# Two ways of control this, using node.action(status)
# and using nodepath to make this node to suscribe to a status changer

export (bool) var status = false
# Optional
export (NodePath) var listen_to
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if(listen_to):
		var target_instance: Node2D = get_node(listen_to)
		assert(target_instance.has_signal("status_changed"))
		get_node(listen_to).connect("status_changed", self, "status_changed_listener")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	frame_coords.y = 1 if status else 0


# Set status
func action(new_status):
	status = new_status

func status_changed_listener(new_status: bool):
	status = new_status
